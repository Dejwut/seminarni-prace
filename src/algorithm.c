#include <linux/module.h>

int init_module(void)

{
  char str[5] = "test";
  char outputChar = 0;
  bool binaryOutput[7];

  // printk("%s", str);

  for (int i = 0; i < sizeof(str); i++) {
    char character = str[i];
    // printk("Character: %d", character);
    // printk("Output character: %d", outputChar);
    outputChar = outputChar ^ character;
    // printk("New output character: %d", outputChar);
  }

  for (int i = 1; i < 8; i++) {
    if (1 == (!!((outputChar << i) & 0x80))) {
      binaryOutput[i - 1] = true;
    } else {
      binaryOutput[i - 1] = false;
    }
  }

  for (int i = 0; i < sizeof(binaryOutput); i++) {
    if (binaryOutput[i]) {
      printk("1");
    } else {
      printk("0");
    }
  }
  return 0;
}

void cleanup_module(void)

{}

MODULE_LICENSE("GPL");
