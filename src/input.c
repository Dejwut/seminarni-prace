#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kernel.h>

#define DEVICES 1
#define DEVICE_MAJOR 69

static int lgen_open(struct inode *inode, struct file *file);
static int lgen_release(struct inode *inode, struct file *file);
static long lgen_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static ssize_t lgen_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *offset);

static const struct file_operations lgen_fops = {.owner = THIS_MODULE,
                                                 .write = lgen_write,
                                                 .open = lgen_open,
                                                 .release = lgen_release,
                                                 .unlocked_ioctl = lgen_ioctl};

struct cdev lgen_cdev;
static int lgen_major = 0;
static struct class *lgen_class = NULL;

int init_lgen(void) {
  int err;

  err = register_chrdev_region(MKDEV(DEVICE_MAJOR, 0), DEVICES, "lgen");
  if (err != 0) {
    return err;
  }
  cdev_init(&lgen_cdev, &lgen_fops);

  cdev_add(&lgen_cdev, MKDEV(DEVICE_MAJOR, 0), DEVICES);

  return 0;
}

void clean_lgen(void) {
  cdev_del(&lgen_cdev);
  unregister_chrdev_region(MKDEV(DEVICE_MAJOR, 0), DEVICES);
}

static int lgen_open(struct inode *inode, struct file *file) {
  printk("lgen: device was opened\n");
  return 0;
}

static int lgen_release(struct inode *inode, struct file *file) {
  printk("lgen: device was closed\n");
  return 0;
}

static long lgen_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
  printk("lgen: device received ioctl command\n");
  return 0;
}

static ssize_t lgen_write(struct file *file, const char __user *buf,
                          size_t count, loff_t *offset) {

  uint8_t data[count];
  char str[count];
  copy_from_user(data, buf, count);

  data[count] = 0;
  for (int i = 0; i < sizeof(data) - 1; i++) {
    str[i] = (char)data[i];
  }

  char outputChar = 0;
  bool binaryOutput[7];

  for (int i = 0; i < sizeof(str); i++) {
    char character = str[i];
    outputChar = outputChar ^ character;
  }

  for (int i = 1; i < 8; i++) {
    if (1 == (!!((outputChar << i) & 0x80))) {
      binaryOutput[i - 1] = true;
    } else {
      binaryOutput[i - 1] = false;
    }
  }

  for (int i = 0; i < sizeof(binaryOutput); i++) {
    if (binaryOutput[i]) {
      printk("1");
    } else {
      printk("0");
    }
  }
  return count;
}

module_init(init_lgen);
module_exit(clean_lgen);

MODULE_LICENSE("GPL");
