.ONESHELL:

SRCPWD := $(CURDIR)/src

run: pack format
	qemu-system-x86_64 -kernel linux/arch/x86_64/boot/bzImage \
  	-initrd ./initramfs.cpio.gz -nographic \
  	-append "console=ttyS0"

clean:
	cd src
	make -C ../linux M=$(SRCPWD) clean

clean_initramfs:
	cd ./initramfs 
	rm -f *.ko
pack: build clean_initramfs
	make -C ./src build $@
	cp ./src/*.ko ./initramfs
	cd initramfs
	chmod +x ./init
	find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../initramfs.cpio.gz


build: 
	cd src
	make -C ../linux M=$(SRCPWD) modules

format:
	clang-format -i $(SRCPWD)/*.c
